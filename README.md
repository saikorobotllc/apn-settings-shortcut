# Apn Settings Shortcut

Apn Settings Shortcut creates a shortcut to open 'APN Settings' screen.

# Usage

1.  Launch app. 
### Launch app image
![Launch App image](images/img01.png)

2.  Click `CREATE APN SETTINGS SHORTCUT` button.
3.  You can get APN settings shortcut into homescreen.
### homescreen
![Home screen](images/img02.png)

# License

MIT