/**
 * The MIT License (MIT)
 * Copyright (c) 2016 Masaki Natsuki
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package com.saikorobot.apnsettingsshortcut;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.Button;
import android.widget.TextView;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {


    @Bind(R.id.message)
    TextView messageText;

    @Bind(R.id.btn_create_shortcut)
    Button createButton;

    boolean isAlias;

    Intent shortCutIntent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        isAlias = isAlias(getIntent());

        setResult(RESULT_CANCELED);

        shortCutIntent = createShortcutIntent(this);
        boolean available = shortCutIntent != null;
        messageText.setText(available ? R.string.message_press_create_button_to_create_shortcut
                : R.string.error_cant_create_shortcut);
        createButton.setEnabled(available);
    }

    @OnClick(R.id.btn_create_shortcut)
    public void onClickCreateShortcut() {
        if (isAlias) {
            setResult(RESULT_OK, shortCutIntent);
        } else {
            sendBroadcast(shortCutIntent);
        }
        finish();
    }

    @Nullable
    private static Intent createShortcutIntent(Context context) {
        Intent intent = new Intent(Settings.ACTION_APN_SETTINGS);
        PackageManager pm = context.getPackageManager();
        List<ResolveInfo> infoList = pm
                .queryIntentActivities(intent, PackageManager.MATCH_DEFAULT_ONLY);
        if (infoList.size() == 0) {
            return null;
        }
        ResolveInfo info = infoList.get(0);
        Intent shortcutIntent = new Intent("com.android.launcher.action.INSTALL_SHORTCUT");
        shortcutIntent.putExtra(Intent.EXTRA_SHORTCUT_NAME, info.loadLabel(pm));
        shortcutIntent.putExtra(Intent.EXTRA_SHORTCUT_ICON_RESOURCE, info.getIconResource());
        shortcutIntent.putExtra(Intent.EXTRA_SHORTCUT_INTENT, intent);

        return shortcutIntent;
    }


    private static boolean isAlias(Intent intent) {
        return !intent.getComponent().getClassName().equals(MainActivity.class.getName());
    }
}
